/* Auto-generated -- DO NOT EDIT */
#ifndef _VERSION_H
#define _VERSION_H

#define VERSION_STR \
"\r\n*** Welcome to WileyMUD III, Quixadhal's Version 1.190w3-beta (15.01.16) ***\r\n"
#define VERSION_BASE "WileyMUD III"
#define VERSION_BUILD "1.190b"
#endif /* _VERSION_H */
